import 'dart:typed_data';
import 'package:flutter_libserialport/flutter_libserialport.dart';
import 'package:thermal_printer_utils/thermal_printer_utils.dart';

// import 'package:esc_pos_bluetooth/esc_pos_bluetooth.dart';


class ThermalPrinterUtils {
  
  // static List<PrinterBluetooth> getBluetoothPrinter() {
  //   return [];
  // }

  static void print() {

    // PrinterBluetoothManager printerManager = PrinterBluetoothManager();

    // printerManager.scanResults.listen((printers) {
    //   log(printers.toString());
    // });

    // printerManager.startScan(const Duration(milliseconds: 2));


  }

  static printViaSerialPort({
    required SerialPort port,
    required List<int> ticketBytes,
    int chunkSizeBytes = 20,
    int queueSleepTimeMs = 20
  }) async {

    port.openWrite();

    final len = ticketBytes.length;

    for (var i = 0; i < len; i += chunkSizeBytes) {
      var end = (i + chunkSizeBytes < len) ? i + chunkSizeBytes : len;
      var bytes = (ticketBytes.sublist(i, end));
      port.write(Uint8List.fromList(bytes), timeout: queueSleepTimeMs);
    }

  }


}

